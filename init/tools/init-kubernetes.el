(require 'use-package)
(require 'aws-vault)

;; TODO: Get magit-section from MELPA stable. This might silence an error when summoning the
;; kubernetes overview.
(defvar kubernetes/configurations
  (let ((default-directory "/home/bart/.kube"))
    `(("test" . ,(expand-file-name "test-eks.yaml"))
      ("production" . ,(expand-file-name "production.yaml")))))

(defun kubernetes/set-config (name)
  (interactive
   (let ((names (mapcar 'car kubernetes/configurations)))
     (list (completing-read "Choose kubernetes configuration: " names))))
  (let ((path (cdr (assoc name kubernetes/configurations))))
    (setenv "KUBECONFIG" path)
    (message "Kubernetes configuration set to %s" path)))

(defun kubernetes/overview (arg)
  (interactive "P")
  (if (or arg (not (getenv "AWS_SESSION_TOKEN")))
      (call-interactively 'aws-vault/assume-role))
  (if (not (get-buffer "*kubernetes overview*"))
      (call-interactively 'kubernetes/set-config))
  (kubernetes-overview))

(use-package kubernetes
  :init
  (fset 'k8s 'kubernetes/overview)
  :bind
  ("C-c C-k c" . kubernetes-display-config)
  ("C-c C-k o" . kubernetes/overview)
  (:map kubernetes-mode-map
        ("C-c l" . kubernetes-logs)
        ("C-c d" . kubernetes-describe)
        ("C-c r" . kubernetes-pods-refresh-now))
  :custom
  (kubernetes-poll-frequency 3600)
  (kubernetes-redraw-frequency 3600)
  (kubernetes-default-overview-namespace "ai")
  (kubernetes-default-overview-view 'pods)
  :commands
  (kubernetes-overview))

(use-package kubernetes-evil
  :after kubernetes)

(provide 'init-kubernetes)
