(use-package evil
  :demand t

  :init
  (setq evil-want-abbrev-expand-on-insert-exit nil
        evil-want-integration t
        evil-want-keybinding nil)

  :config
  (evil-mode t)
  (define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
  (define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)
  (define-key evil-normal-state-map (kbd "C-n") nil)
  (define-key evil-normal-state-map (kbd "C-p") nil)
  (setq-default evil-move-cursor-back nil)

  (use-package evil-collection
    :demand t
    :pin melpa
    :diminish evil-collection-unimpaired-mode
    :custom (evil-collection-setup-minibuffer t)
    :config
    (setq forge-add-default-bindings nil)
    (evil-collection-init))

  (use-package evil-surround
    :demand t
    :config (global-evil-surround-mode)))

(use-package yasnippet
  :demand t
  :diminish yas-minor-mode
  :config
  (use-package helm-c-yasnippet
    :bind ("C-c y" . helm-yas-complete))
  (use-package yasnippet-snippets)
  (global-set-key (kbd "C-c y") 'helm-yas-complete)
  (yas-global-mode))

(setq-default tab-width 4
              indent-tabs-mode nil
              fill-column 100)

(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-x M-o") 'delete-blank-lines)

(provide 'init-editing)
