(require 'use-package)
(require 'browse-url)
(require 'projectile)

(setq inhibit-startup-message t
      backup-directory-alist '(("." . "~/.emacs.d/backup"))
      initial-scratch-message nil scroll-preserve-screen-position nil
      scroll-conservatively 101
      scroll-margin 20
      auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
      browse-url-generic-program "/usr/bin/firefox"
      browse-url-browser-function 'browse-url-generic
      ad-redefinition-action 'accept)

(global-set-key (kbd "C-x s") nil)
(global-set-key (kbd "C-x C-o") nil)
(global-set-key (kbd "C-x M-s") 'save-some-buffers)

(use-package ace-window
  :bind
  (("M-o" . ace-window))
  :custom
  (aw-dispatch-always t))

(use-package buffer-move
  :bind (("C-x [" . buf-move-left)
         ("C-x ]" . buf-move-right)))

(which-function-mode 1)

(provide 'init-base)
