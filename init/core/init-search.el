(use-package helm-swoop
  :ensure t
  :bind
  (("C-c s" . helm-multi-swoop-all)
   ("C-s" . helm-swoop)
   ("C-r" . helm-resume)))

(use-package helm-ag
  :ensure t
  :bind (("C-c a" . helm-ag)))

(use-package ripgrep
  :config
  (use-package helm-rg
    :pin melpa))

(provide 'init-search)
