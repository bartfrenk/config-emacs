(use-package material-theme)

(use-package atom-one-dark-theme)

(use-package smart-mode-line
  :demand t
   :init
   (setq sml/no-confirm-load-theme t
         sml/theme 'respectful)
   :config
   (use-package smart-mode-line-atom-one-dark-theme
     :pin melpa
     :demand t)
   (sml/setup))

(use-package smooth-scrolling)

(use-package rainbow-delimiters
  :functions color-saturate-name
  :init
  (add-hook 'prog-mode-hook (lambda ()
                              (rainbow-delimiters-mode)))
  :config
  (add-hook 'after-load-theme-hook
            (lambda ()
              (setq rainbow-delimiters-outermost-only-face-count 1)
              (set-face-attribute 'rainbow-delimiters-depth-1-face nil
                                  :foreground 'unspecified
                                  :weight 'bold))))

(use-package whitespace
  :demand t
  :diminish global-whitespace-mode
  :config
  (global-whitespace-mode t)
  (setq whitespace-style '(face tab-mark lines-tail trailing)
        whitespace-line-column 100
        ;; Move to mode configurations
        whitespace-global-modes '(not latex-mode
                                      org-mode
                                      web-mode
                                      dockerfile-mode
                                      mhtml-mode
                                      nxml-mode
                                      magit-status-mode)))

(defun ui/rainbow-delimiters-saturate (percent)
  "Saturate rainbow delimiters by specified percentage."
  (interactive "nPercentage: ")
  (dolist (index (number-sequence 1 rainbow-delimiters-max-face-count))
    (let* ((face (intern (format "rainbow-delimiters-depth-%d-face" index)))
           (old-color (face-foreground face)))
      (unless (null old-color)
        (let ((new-color (color-saturate-name old-color percent)))
          (set-face-foreground face new-color))))))

(defun ui/switch-theme (theme)
  (interactive
   (list
    (intern (completing-read "Load custom theme: "
                             (mapcar 'symbol-name
                                     (custom-available-themes))))))
  (mapc #'disable-theme custom-enabled-themes)
  (load-theme theme t))

(defun ui/set-dark-theme ()
  (interactive)
  (load-theme 'material t)
  (ui/switch-theme 'atom-one-dark)
  (load-theme 'smart-mode-line-atom-one-dark))

(add-to-list 'default-frame-alist '(font . "Source Code Pro 8"))

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(blink-cursor-mode t)
(global-hl-line-mode 1)
(show-paren-mode 1)

(fset 'yes-or-no-p 'y-or-n-p)
(column-number-mode -1)

(setq-default line-spacing '0.5)

(global-set-key (kbd "C-c C--") 'linum-mode)

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (select-frame frame)
                (ui/set-dark-theme)))
  (ui/set-dark-theme))


(provide 'init-ui)
