(require 'use-package)

(use-package company
  :demand t
  :bind
  (("<M-tab>" . company-complete)
   :map company-active-map
   ("C-d" . company-show-doc-buffer)
   ("C-h" . delete-backward-char))
  :config
  (setq company-tooltip-limit 20
        company-idle-delay 0
        company-tooltip-align-annotations t
        company-minimum-prefix-length 1
        company-echo-delay 0
        company-begin-commands '(self-insert-command))
  (add-to-list 'company-transformers 'company-sort-prefer-same-case-prefix)
  (global-company-mode)
  :diminish company-mode)

(provide 'init-company)
