(provide 'init-misc)

(use-package ace-window
  :bind ("M-m" . ace-window))

(use-package diminish)

(use-package eldoc
  :diminish eldoc-mode)

(use-package fringe-helper)

(use-package paredit
  :pin melpa
  :config
  (use-package evil-paredit
    :pin melpa))

(use-package autorevert
  :diminish auto-revert-mode
  :ensure nil
  :hook (doc-view-mode . (lambda () (auto-revert-mode t))))

;; Triage

(use-package edit-indirect)
