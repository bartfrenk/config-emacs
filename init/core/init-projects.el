(require 'init-constants)
(require 'evil)

(use-package projectile
  :demand t
  :config
  (setq projectile-completion-system 'helm
        projectile-enable-caching t
        projectile-cache-file (expand-file-name "projectile.cache" cache-dir)
        projectile-globally-ignored-file-suffixes '(".png" ".gif" ".pdf"))
  :bind (:map projectile-mode-map
              ("C-c p i" . projectile-invalidate-cache)
              ("C-c p o" . projectile-find-file-other-window))
  :diminish projectile-mode
  :config

  (use-package helm-projectile
    :demand t
    :pin melpa
    :config
    (setq projectile-switch-project-action 'projectile-find-file)
    (helm-projectile-on)
    :bind (:map projectile-mode-map
                ("C-c p s" . helm-projectile-ag)
                ("C-c p p" . helm-projectile-switch-project)
                ("C-c p f" . helm-projectile-find-file)))

  (use-package projectile-ripgrep
    :after (projectile ripgrep)
    :bind (:map projectile-mode-map
                ("C-c p r" . helm-projectile-rg)))

  (projectile-mode)

  (use-package neotree
    :bind
    :demand t
    :after projectile
    :config
    (setq neo-window-width 40
          neo-autorefresh nil)
    (evil-define-key '(normal visual) neotree-mode-map "o" 'neotree-enter-ace-window)
    (add-to-list 'neo-hidden-regexp-list "\\.o$")
    (add-to-list 'neo-hidden-regexp-list "\\.so$")
    (add-to-list 'neo-hidden-regexp-list "\\.hi$")
    (add-to-list 'neo-hidden-regexp-list "__pycache__$")
    (defun neotree-project-dir ()
        "Open NeoTree using the git root."
        (interactive)
        (let ((project-dir (projectile-project-root))
            (file-name (buffer-file-name)))
        (neotree-toggle)
        (if project-dir
            (if (neo-global--window-exists-p)
                (progn
                    (neotree-dir project-dir)
                    (neotree-find file-name)))
            (message "Could not find git project root.")))))

  (global-set-key [f8] `neotree-project-dir))



(provide 'init-projects)
