(defvar cache-dir (expand-file-name ".cache/" user-emacs-directory)
  "This folder stores automatically generated persistent session files.")

;; maybe also define shortcut keys here, e.g.
;; "C-c C-a" autoformat buffer key
;; "C-c C-l" source buffer
;; "M-]" goto-definition
;; "M-[" pop-definition
;; etc...

(provide 'init-constants)
