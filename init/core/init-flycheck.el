(require 'use-package)

(use-package flycheck
  :demand t
  :bind
  (:map flycheck-mode-map
        ("M-n" . flycheck-next-error)
        ("M-p" . flycheck-previous-error)
        ("C-c ! h" . helm-flycheck))
  :config
  (setq-default flycheck-emacs-lisp-load-path 'inherit
                flycheck-check-syntax-automatically '(save mode-enabled)
                flycheck-standard-error-navigation nil
                flycheck-checker-error-threshold nil)
  (setq flycheck-global-modes '(not org-mode))

  (global-flycheck-mode))

(provide 'init-flycheck)
