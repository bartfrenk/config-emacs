(global-git-gutter-mode)

(use-package git-gutter
  :diminish git-gutter-mode)

(use-package magit-section)

(use-package magit
  :demand t
  :config
  (use-package magit-org-todos
    :config
    (magit-org-todos-autoinsert))
  (use-package magit-todos
    :hook (magit-mode . (lambda () (magit-todos-mode t))))
  (use-package forge
    :pin melpa
    :config
    (setq forge-add-default-bindings nil)
    (use-package ghub
      :pin melpa)))

(provide 'init-vc)

