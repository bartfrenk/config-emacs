(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#ffffff" "#f36c60" "#8bc34a" "#fff59d" "#4dd0e1" "#b39ddb" "#81d4fa" "#263238"))
 '(custom-safe-themes
   '("ed68393e901a88b9feefea1abfa9a9c5983e166e4378c71bb92e636423bd94fd" default))
 '(fci-rule-color "#37474f")
 '(helm-minibuffer-history-key "M-p")
 '(hl-sexp-background-color "#1c1f26")
 '(package-selected-packages
   '(lsp-haskell js2-mode solidity-flycheck company-solidity solidity-mode tide prettier-js typescript-mode haskell-mode ob-sql-mode alert kubernetes-evil kubernetes forge magit-todos magit-org-todos magit magit-section lsp-ui lsp-jedi lsp-mode deft format-sql python-docstring virtualenvwrapper org-roam org-babel-mode org-babel yasnippet-snippets yapfify yaml-mode web-mode use-package sqlformat smooth-scrolling smart-mode-line-atom-one-dark-theme rainbow-delimiters pyenv-mode py-isort projectile-ripgrep ob-http neotree material-theme jinja2-mode helm-swoop helm-rg helm-projectile helm-flycheck helm-c-yasnippet helm-ag gnuplot git-gutter fringe-helper flycheck-mypy evil-surround evil-paredit evil-collection edit-indirect dotenv-mode dockerfile-mode diminish buffer-move blacken atom-one-dark-theme ace-window))
 '(safe-local-variable-values
   '((eval venv-workon "miniscule")
     (TeX-master . t)
     (haskell-process-args-ghci "ghci")
     (haskell-process-path-ghci . "stack")
     (flycheck-python-mypy-config quote
                                  ("pyproject.toml"))
     (eval flycheck-select-checker 'python-mypy)))
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#f36c60")
     (40 . "#ff9800")
     (60 . "#fff59d")
     (80 . "#8bc34a")
     (100 . "#81d4fa")
     (120 . "#4dd0e1")
     (140 . "#b39ddb")
     (160 . "#f36c60")
     (180 . "#ff9800")
     (200 . "#fff59d")
     (220 . "#8bc34a")
     (240 . "#81d4fa")
     (260 . "#4dd0e1")
     (280 . "#b39ddb")
     (300 . "#f36c60")
     (320 . "#ff9800")
     (340 . "#fff59d")
     (360 . "#8bc34a")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
