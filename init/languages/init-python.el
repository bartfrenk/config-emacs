(require 'subr-x)
(require 'flycheck)
(require 'lsp)
(require 'use-package)
(require 'lsp-ui)

;; TODO: isort and black on save

(use-package virtualenvwrapper
  :config
  (setq venv-location "/home/bart/.pyenv/versions"))

(defun python--locate-version-file ()
  (let* ((name ".python-version")
         (dir (locate-dominating-file
               (buffer-file-name)
               (lambda (dir)
                 (file-exists-p (concat dir name))))))
    (when dir (concat dir name))))

(defun python--read-file (file)
  (with-temp-buffer
    (insert-file-contents file)
    (buffer-string)))

(defun python/set-dominating-venv ()
  (interactive)
  (if-let ((file (python--locate-version-file)))
      (let ((venv-name (string-trim (python--read-file file))))
        (message "Setting virtualenv to %s" venv-name)
        (venv-workon venv-name))
    (message "No dominating venv found")))

(defun pipenv/replace-versions ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "[>=~]=\\([0-9]+\.[0-9]+\\).[0-9]+" nil t)
      (replace-match "~=\\1" t))))

(defun python/init ()
  (when (fboundp 'auto-complete-mode)
    (auto-complete-mode -1))
  (lsp-deferred)
  (lsp-ui-doc-mode -1)
  (lsp-ui-sideline-mode -1)
  (lsp-completion-mode t)
  (lsp-flycheck-add-mode 'python-mode)
  (lsp-headerline-breadcrumb-mode t)
  (python/set-dominating-venv)
  ;; This doesn't work yet:
  ;; see https://emacs.stackexchange.com/questions/5452/before-save-hook-for-cc-mode
  (add-hook 'python-mode-hook
            (lambda ()
              (add-hook 'before-save-hook 'python/format-buffer nil 'local))))


(defun python/set-checkers ()
  (let ((enabled-checkers '(python-pycompile python-mypy python-pylint))
        (disabled-checkers '(python-pyright python-flake8)))
    (dolist (checker (append disabled-checkers enabled-checkers))
      (delete checker flycheck-checkers))
    (nconc flycheck-checkers enabled-checkers))
  (flycheck-remove-next-checker 'python-pylint 'python-mypy)
  (flycheck-remove-next-checker 'python-pycompile 'python-mypy)
  (flycheck-add-next-checker 'python-mypy '(t . python-pylint))
  (flycheck-add-next-checker 'python-pycompile' (warning . python-mypy)))

(defun python/format-buffer ()
  "Format a Python buffer"
  (interactive)
  (blacken-buffer)
  (py-isort-buffer))

(use-package python
  :commands python-shell-send-string
  :hook (python-mode . python/init)
  :custom
  (flycheck-python-pylint-executable "pylint")
  :bind
  (:map python-mode-map
        ("C-c C-a" . python/format-buffer)
        ("C-c M-j" . run-python)
        ("C-c C-k" . python-shell-import-package))

  :config
  (use-package flycheck-mypy
    :pin melpa
    :config
    (python/set-checkers)))

(use-package blacken
  :pin melpa
  ;;:hook (python-mode . blacken-mode)
  )

(use-package python-docstring
  :pin melpa
  :hook (python-mode . python-docstring-mode))

(use-package lsp-jedi
  :custom
  (lsp-jedi-executable-command "/home/bart/.pyenv/versions/jedi/bin/jedi-language-server")
  :config
  (require 'lsp-mode)
  (with-eval-after-load "lsp-mode"
    (add-to-list 'lsp-disabled-clients 'pylsp)
    (add-to-list 'lsp-enabled-clients 'jedi)))

(provide 'init-python)
