(require 'whitespace)

(use-package conf-mode
  :ensure nil
  :mode (("\\Pipfile\\'" . conf-toml-mode)
         ("\\.yapf\\'" . confmode)))

(use-package dockerfile-mode)

(use-package dotenv-mode)

(use-package gnuplot)

(use-package markdown-mode)

(use-package yaml-mode
  :init
  (add-hook 'yaml-mode-hook
            (lambda ()
              (setq whitespace-style '(face tab-mark trailing))))
  :mode
  ("\\.raml\\'" . yaml-mode))

(use-package jinja2-mode)

(provide 'init-modes)

