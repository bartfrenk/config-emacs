(require 'use-package)
(require 'flycheck)

(use-package solidity-mode)

(use-package company-solidity
  :after solidity-mode)

(use-package solidity-flycheck
  :pin melpa
  :after solidity-mode
  :init
  (setq solidity-flycheck-solium-checker-active t
        solidity-flycheck-solc-checker-active t)
  :custom
  (flycheck-solidity-solium-soliumrcfile "/home/bart/.soliumrc.json"))

(provide 'init-solidity)
