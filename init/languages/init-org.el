(require 'use-package)
(require 'flycheck)
(require 'org-agenda)
(use-package ob-http)
(use-package ob-sql-mode :pin melpa)


(defun org-babel/disable-local-checkers ()
  "Disable specific checkers in org source blocks."
  (setq-local flycheck-disabled-checkers '(emacs-lisp-checkdoc
                                           python-pylint
                                           python-pycompile)))

(use-package org
  :demand t
  :ensure nil
  :bind
  (("C-c l" . org-store-link)
   ("C-c a" . org-agenda) ("C-c c" . org-capture)
   ("C-c M-p" . org-latex-export-to-pdf))
  (:map org-mode-map
        ("M-[" . org-mark-ring-goto)
        ("M-]" . org-open-at-point))

  :custom
  (org-clock-persist t)
  (org-agenda-show-all-dates t)
  (org-agenda-start-on-weekday 1)
  (org-agenda-todo-ignore-scheduled 'all)
  (org-agenda-span 14)
  (org-babel-load-languages '((emacs-lisp . t)
                              (sql . t)
                              (shell . t)
                              (http . t)
                              (maxima . t)
                              (plantuml . t)
                              (gnuplot . t)))
  (org-confirm-babel-evaluate nil)
  (org-edit-src-content-indentation 0)
  (org-hide-leading-stars t)
  (org-log-done nil)
  (org-outline-path-complete-in-steps nil)
  (org-src-fontify-natively t)
  (org-startup-folded 'content)
  (org-startup-with-inline-images t)
  (org-tags-column -100)
  (org-use-tag-inheritance nil)
  (org-startup-indented t)
  (org-image-actual-width 400)

  :config

  (defun org-fill-paragraph--latex-environment (&rest args)
    "Use default fill-paragraph in latex environments."
    (not (eql (org-element-type (org-element-context)) 'latex-environment)))

  (advice-add 'org-fill-paragraph :before-while
              #'org-fill-paragraph--latex-environment)

  ;; allow emphasis to extend over two lines
  (setcar (nthcdr 4 org-emphasis-regexp-components) 4)
  (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)
  (setq org-time-stamp-formats '("<%Y-%m-%d>" . "<%Y-%m-%d %H:%M>"))

  :hook ((org-src-mode . org-babel/disable-local-checkers)
         (org-babel-after-execute . org-display-inline-images)))

 (defun org/time-stamp ()
    "Inserts an inactive timestamp of the current time."
    (interactive)
    (org-time-stamp '(16) t))

(use-package tasks
  :demand t
  :ensure nil
  :commands (tasks/init)
  :bind
  (("C-c t i" . tasks/open-inbox)
   ("C-c t a" . tasks/open-actions))
  :config
  (tasks/init "/home/bart/documents/notes/gtd"))

;; TODO: Use helm
(use-package org-roam
  :demand t
  :ensure nil
  :bind
  (("C-c r r" . org-roam-buffer-toggle)
   ("C-c r b" . org-roam-buffer-display-dedicated)
   ("C-c r i" . org-roam-node-insert)
   ("C-c r c" . org-roam-capture)
   ("C-c r /" . org-roam-node-find))
  :init
  (setq org-roam-directory (file-truename "~/documents/notes/personal/roam"))
  :after org
  :config
  (org-roam-db-autosync-enable))

(use-package org-download
  :ensure nil)

(use-package deft
  :after org
  :bind
  ("C-c r d" . deft)
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-directory org-roam-directory))

(use-package journal
  :demand t
  :ensure nil
  :commands (journal/init)
  :bind
  (("C-c j" . journal/open)))

(journal/init "~/documents/notes/personal/journals")

(provide 'init-org)
