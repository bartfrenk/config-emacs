(require 'lsp-ui)

(defun haskell/init ()
  (interactive)
  (lsp)
  (lsp-ui-doc-mode t)
  (lsp-ui-sideline-mode -1)
  (lsp-completion-mode t)
  (lsp-headerline-breadcrumb-mode -1))

(setq lsp-enabled-clients nil)

(use-package haskell-mode
  :after lsp-mode
  :custom
  (haskell-stylish-on-save t)
  :hook (haskell-mode . haskell/init)
  :config
  (add-to-list 'lsp-enabled-clients 'lsp-haskell))



(use-package lsp-haskell
  :pin melpa)

(provide 'init-haskell)

