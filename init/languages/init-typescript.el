(require 'flycheck)

(defun typescript/init ()
  (setq tab-width 2))

(defun tide/init ()
  (interactive)
  (tide-setup)
  (eldoc-mode 1)
  (tide-hl-identifier-mode 1))

(defvar typescript/project-dir
  "/home/bart/dev/ai/components/big-intel-google-sheets-add-on/clasp")

(use-package tide
  :ensure t
  :after (typescript-mode company flycheck)
  :bind (:map tide-mode-hook
              ("M-]" . tide-jump-to-definition))
  :hook ((typescript-mode . tide/init)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(use-package typescript-mode
  :hook ((typescript-mode . typescript/init))
  :config
  (setq typescript-indent-level 2
        flycheck-javascript-eslint-executable
        (concat typescript/project-dir "/node_modules/eslint/bin/eslint.js")
        flycheck-eslint-args `("--config" ,(concat typescript/project-dir "/.eslintrc")))
  (flycheck-add-mode 'javascript-eslint 'typescript-mode))

(defun typescript/node ()
  (interactive)
  (message (shell-command-to-string "which node")))

(use-package prettier-js
  :pin "melpa"
  :hook ((typescript-mode . prettier-js-mode))
  :diminish prettier-js-mode
  :custom
  (prettier-js-command  (concat typescript/project-dir "/node_modules/prettier/bin-prettier.js"))
  (prettier-js-args `("--config" ,(concat typescript/project-dir "/.prettierrc")))
  :config
  (setq js-indent-level typescript-indent-level
        js2-indent-level typescript-indent-level
        jsx-indent-level typescript-indent-level
        sgml-basic-offset typescript-indent-level
        js2-basic-offset typescript-indent-level))



(provide 'init-typescript)
