
(use-package js2-mode
  :bind (:map js2-mode-map
              ("M-]" . tern-find-definition)
              ("M-[" . tern-pop-find-definition)
              ("C-c C-l" . skewer-load-buffer)
              ("C-M-x" . skewer-eval-defun)
              ("C-c C-d" . helm-dash-at-point))
  :config
  (add-hook 'js-mode-hook (lambda () (prettier-js-mode))))

(let ((indent-level 2))
    (setq js-indent-level indent-level
          js2-basic-offset indent-level))

(provide 'init-javascript)

