(require 'paredit)
(require 'flycheck)
(require 'eldoc)

(defun elisp/init ()
  (eldoc-mode 1)
  (paredit-mode 1)
  (evil-paredit-mode 1))

(add-hook 'emacs-lisp-mode-hook 'elisp/init)

(define-key emacs-lisp-mode-map (kbd "M-]") 'xref-find-definitions)
(define-key emacs-lisp-mode-map (kbd "M-[") 'xref-pop-marker-stack)
(delete 'emacs-lisp-checkdoc flycheck-checkers)

(provide 'init-elisp)
