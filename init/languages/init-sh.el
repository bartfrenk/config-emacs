(require 'use-package)
(require 'flycheck)

(use-package sh-script
  :hook (sh-mode-hook . (flycheck-select-checker 'sh-posix-bash))
  :custom
  (sh-basic-offset 2)
  (smie-indent-basic 2)
  (flycheck-shellcheck-excluded-warnings ("SC1090"   ;; No warnings on non-constant sources
                                          "SC2039")) ;; No warnings on sh incompatible constructions
  (flycheck-shellcheck-follow-sources nil))

(provide 'init-sh)
