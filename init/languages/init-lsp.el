(use-package lsp-mode
  :custom
  (lsp-keymap-prefix "C-c ;")
  (lsp-auto-configure nil)
  (lsp-completion-enable t)
  (lsp-enable-imenu t)
  :config
  (setq lsp-enabled-clients nil)
  (define-key lsp-mode-map (kbd "C-c ;") lsp-command-map)

  (use-package lsp-ui
    :bind
    (:map lsp-mode-map
          ("C-c ; D" . lsp-ui-doc-show)
          ("C-c ; d" . lsp-ui-doc-glance)
          ("C-c ; i" . lsp-ui-imenu)
          ("C-u C-c ; D" . lsp-ui-doc-hide)
          ("M-]" . lsp-ui-peek-find-definitions)
          ("M-[" . xref-pop-marker-stack))))

(provide 'init-lsp)
