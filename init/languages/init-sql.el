(require 'use-package)
(require 'diminish)
(require 'sql)

(use-package format-sql
  :bind
  (:map sql-mode-map
        ("C-c C-a" . format-sql-buffer)))

(provide 'init-sql)
