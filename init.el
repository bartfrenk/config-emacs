;; See https://emacs.stackexchange.com/questions/55324/how-to-install-latest-version-of-org-mode
(add-to-list 'load-path (expand-file-name "site/org-mode/lisp" user-emacs-directory))
(require 'org-loaddefs)

;; TODO: Try visual-line-mode
;; TODO: Set up forge with gitlab
(require 'init-main (expand-file-name "init/init-main.el" user-emacs-directory))
