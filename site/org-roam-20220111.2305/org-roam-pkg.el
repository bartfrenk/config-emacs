(define-package "org-roam" "20220111.2305" "A database abstraction layer for Org-mode"
  '((emacs "26.1")
    (dash "2.13")
    (f "0.17.2")
    (org "9.4")
    (emacsql "3.0.0")
    (emacsql-sqlite "1.0.0")
    (magit-section "3.0.0"))
  :commit "86c908536320201b721c4805407be19c31f0dd54" :authors
  '(("Jethro Kuan" . "jethrokuan95@gmail.com"))
  :maintainer
  '("Jethro Kuan" . "jethrokuan95@gmail.com")
  :keywords
  '("org-mode" "roam" "convenience")
  :url "https://github.com/org-roam/org-roam")
;; Local Variables:
;; no-byte-compile: t
;; End:
