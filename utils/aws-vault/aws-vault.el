(defvar aws-vault/config "/home/bart/.aws.vault/config")

(defvar aws-vault/profile "developer")

;; Not strictly necessary for AWS vault
(defvar aws-vault/region "eu-west-1")

(defun aws-vault--set-session-envs (credentials)
  (setenv "AWS_ACCESS_KEY_ID" (gethash "AccessKeyId" credentials))
  (setenv "AWS_SESSION_TOKEN" (gethash "SessionToken" credentials))
  (setenv "AWS_SECRET_ACCESS_KEY" (gethash "SecretAccessKey" credentials))
  (setenv "AWS_DEFAULT_REGION" aws-vault/region)
  (message "Session valid until %s " (gethash "Expiration" credentials)))

(defun aws-vault--assume-role (profile mfa-token)
  (setenv "AWS_CONFIG_FILE" aws-vault/config)
  (let* ((cmd (format "aws-vault exec %s --json --mfa-token %s --duration=1h" profile mfa-token))
         (out (shell-command-to-string cmd)))
    (aws-vault--set-session-envs (json-parse-string out))))

(defun aws-vault/assume-role (mfa-token)
  (interactive "sEnter MFA token: ")
  (aws-vault--assume-role aws-vault/profile mfa-token))

(provide 'aws-vault)
