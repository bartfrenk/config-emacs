(require 'miniscule)
(require 'sql)

(defun db/connect ()
  (interactive)
  (let* ((config (call-interactively 'miniscule/get-config))
         (_ (message "config %s" config))
         (db-config (gethash 'database config))
         (_ (message "%s" db-config))
         (sql-database (format "postgresql://%s:%s@localhost:15432/%s"
                               (gethash 'user db-config)
                               (gethash 'password db-config)
                               (gethash 'database db-config))))
    (setq sql-postgres-login-params `((sql-database ,sql-database)))
    (setq sql-connection-alist `((main (sql-product `postgres) (sql-database ,sql-database))))
    (sql-postgres)))

(provide 'db)

