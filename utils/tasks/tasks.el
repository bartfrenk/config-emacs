(require 'org)
(require 'org-refile)
(require 'org-capture)
(require 'org-agenda)
(provide 'tasks)

(defvar tasks/dir)

(defun tasks/open-inbox ()
  (interactive)
  (find-file (concat tasks/dir "/inbox.org"))
  (revert-buffer :ignore-auto :noconfirm))

(defun tasks/open-actions ()
  (interactive)
  (find-file (concat tasks/dir "/actions.org"))
  (revert-buffer :ignore-auto :noconfirm))

(defun tasks/set-agenda-files ()
  (let ((default-directory tasks/dir))
    (setq org-agenda-files
          `(,(expand-file-name "actions.org")
            ,(expand-file-name "tickler.org")
            ,(expand-file-name "calendar.org")))))

(defun tasks/set-refile-targets ()
  (let ((default-directory tasks/dir))
    (setq org-refile-targets
          `((,(expand-file-name "tickler.org") :maxlevel . 2)
            (,(expand-file-name "actions.org") :maxlevel . 2)
            (,(expand-file-name "someday.org") :maxlevel . 1)))))

(defun tasks/add-capture-templates ()
  (add-to-list 'org-capture-templates
               `("t" "Task" entry
                 (file+headline ,(concat tasks/dir "/inbox.org") "Inbox")
                 "* TODO %^{Task}\nDate: %U\n\n%?")))

(defun tasks--has-previous-siblings (pred)
  (let ((found nil))
    (save-excursion
      (while (and (not found) (org-goto-sibling t))
        (when (funcall pred)
          (setq found t))))
    found))

(defun tasks--is-first-sibling (pred)
  (and (funcall pred) (not (tasks--has-previous-siblings pred))))

(defun tasks--current-has-state (state)
  (string= state (org-get-todo-state)))

(defun tasks--current-is-todo ()
  (tasks--current-has-state "TODO"))

(defun tasks/agenda-skip-function ()
  "Skips entries depending on outline"
  (let ((should-keep
         (if (< (org-outline-level) 3) (tasks--current-is-todo)
           (tasks--is-first-sibling 'tasks--current-is-todo))))
    (unless should-keep
      (or (outline-next-heading)
          (goto-char (point-max))))))

(defun tasks/set-agenda-custom-commands ()
  (setq org-agenda-custom-commands
        '(("l" "Laptop" tags-todo "@laptop"
           ((org-agenda-overriding-header "Laptop")
            (org-agenda-skip-function #'tasks/agenda-skip-function)))
          ("p" "Phone" tags-todo "@phone"
           ((org-agenda-overriding-header "Phone")
            (org-agenda-skip-function #'tasks/agenda-skip-function)))
          ("n" "Next steps" todo "TODO"
           ((org-agenda-overriding-header "Phone")
            (org-agenda-skip-function #'tasks/agenda-skip-function))))))


(defun tasks/init (&optional dir)
  (setq tasks/dir dir)
  (tasks/set-agenda-files)
  (tasks/set-refile-targets)
  (tasks/add-capture-templates)
  (tasks/set-agenda-custom-commands))

(provide 'tasks)
