(require 'yaml)
(require 'projectile)
(require' aws-vault)

(defvar miniscule/environments-dir "deployment/overlays/")

(defun miniscule--filter-subdirs (paths)
  (seq-filter (lambda (s) (not (string-match-p (regexp-opt '("." "..")) s))) paths))

(defun miniscule--list-environments ()
  (let ((path (concat (projectile-project-root) miniscule/environments-dir)))
    (miniscule--filter-subdirs (directory-files path))))

(defun miniscule--get-config (path)
  (let* ((cmd (format "CONFIG=%s miniscule" path))
         (out (shell-command-to-string cmd)))
    (message "cmd %s" cmd)
    (message "out %s" out)
    (yaml-parse-string out)))

(defun miniscule/test ()
  (interactive)
  (message "%s" (miniscule--list-environments)))

(defun miniscule/get-config (environment)
  (interactive
   (let ((environments (miniscule--list-environments)))
     (list (completing-read "Choose environment: " environments))))
  (let* ((root (projectile-project-root))
         (path (concat root miniscule/environments-dir environment "/config.yaml"))
         (config (miniscule--get-config path)))
    (message "%s" config)
    config))


(provide 'miniscule)
