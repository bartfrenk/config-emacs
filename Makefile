.PHONY: all org-mode jedi

help: ##
	@fgrep -h "##" $(MAKEFILE_LIST) | \
	fgrep -v fgrep | sed -e 's/: ##/##/' | column -t -s##

all: ##
all: org-mode jedi

org-mode: ##
org-mode:
	make -C site/org-mode autoloads

jedi: ##
jedi:
	make -C tools/jedi all
